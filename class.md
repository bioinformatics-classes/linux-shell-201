### Linux shell 201

![Linux](assets/linux.jpg)

© Francisco Pina Martins 2015-2025

---

### Now that you know basic shell:


* &shy;<!-- .element: class="fragment" -->Scripting
  * &shy;<!-- .element: class="fragment" -->Running
  * &shy;<!-- .element: class="fragment" -->Reading
  * &shy;<!-- .element: class="fragment" -->Writing
* &shy;<!-- .element: class="fragment" -->Privileges
  * &shy;<!-- .element: class="fragment" -->Who
  * &shy;<!-- .element: class="fragment" -->How
* &shy;<!-- .element: class="fragment" -->Package managers
  * &shy;<!-- .element: class="fragment" -->System-level
  * &shy;<!-- .element: class="fragment" -->User level

---

### Scripting

* &shy;<!-- .element: class="fragment" -->Running a script
* &shy;<!-- .element: class="fragment" -->Try to do this on your own:
  * &shy;<!-- .element: class="fragment" -->Get [this script](assets/hello.sh) in your file system
  * &shy;<!-- .element: class="fragment" -->Run it
  * &shy;<!-- .element: class="fragment" -->It's OK if you run into some trouble

<img src="assets/obi-wan.jpg" style="background:none; border:none; box-shadow:none; float:bottom" class="fragment">

|||

### Scripting

```bash
# SPOILER ALERT!
# Scroll below for the solution















wget https://gitlab.com/bioinformatics-classes/linux-shell-201/raw/master/assets/hello.sh
chmod +x hello.sh
./hello.sh

```

---

### Scripting

* &shy;<!-- .element: class="fragment" -->Let's up the game
* &shy;<!-- .element: class="fragment" -->Try to do the same for these scripts
  * &shy;<!-- .element: class="fragment" -->[Python](assets/hello.py) 
  * &shy;<!-- .element: class="fragment" -->[Perl](assets/hello.pl) 
  * &shy;<!-- .element: class="fragment" -->[Ruby](assets/hello.rb) 
  * &shy;<!-- .element: class="fragment" -->[JavaScript](assets/hello.js) 
* &shy;<!-- .element: class="fragment" -->Notice something strange?

<img src="assets/grevious.jpg" style="background:none; border:none; box-shadow:none;" class="fragment">

|||

### Scripting

```bash
# SPOILER ALERT!
# Scroll below for the solution















wget https://gitlab.com/bioinformatics-classes/linux-shell-201/raw/master/assets/hello.py
wget https://gitlab.com/bioinformatics-classes/linux-shell-201/raw/master/assets/hello.pl
wget https://gitlab.com/bioinformatics-classes/linux-shell-201/raw/master/assets/hello.rb
wget https://gitlab.com/bioinformatics-classes/linux-shell-201/raw/master/assets/hello.js
chmod +x hello.*
./hello.py
./hello.pl
./hello.rb  # Something went wrong!
./hello.js  # Something went wrong again!

```

---

### Looking at scripts


* &shy;<!-- .element: class="fragment" -->Look at these scripts using some shell tools
  * &shy;<!-- .element: class="fragment" -->`cat` 
  * &shy;<!-- .element: class="fragment" -->`less` 
  * &shy;<!-- .element: class="fragment" -->`nano` 
  * &shy;<!-- .element: class="fragment" -->`vim` 

<img src="assets/grevious2.jpg" style="background:none; border:none; box-shadow:none;" class="fragment">

|||

### Plot twist!

* &shy;<!-- .element: class="fragment" -->Try to compress all the scripts in a `tar.xz` file
* &shy;<!-- .element: class="fragment" -->You will need:
  * &shy;<!-- .element: class="fragment" -->`tar`
  * &shy;<!-- .element: class="fragment" -->wildcards
* &shy;<!-- .element: class="fragment" -->Try to uncompress them afterwards
* &shy;<!-- .element: class="fragment" --><font color="purple">*Hint*:</font> Create new directories not to clutter your `~/` 


|||

### Plot twist!

```bash
# SPOILER ALERT!
# Scroll below for the solution















tar cvfJ scripts.tar.xz hello.*  # This will compress your files in a tar.xz archive
mkdir compression_tests
mv scripts.tar.xz compression_tests
cd compression_tests
tar xvfJ scripts.tar.xz  # This will uncompress them
```

---

### Shebang (AKA Hashbang)


* &shy;<!-- .element: class="fragment" -->Notice the `#!` in the first line?
  * &shy;<!-- .element: class="fragment" -->This is called a "Shebang"
  * &shy;<!-- .element: class="fragment" -->It tells your shell which program should *interpret* the script
  * &shy;<!-- .element: class="fragment" -->Without it you would need to tell this to the shell
    * &shy;<!-- .element: class="fragment" -->`perl hello.pl`

<a href="https://www.youtube.com/watch?v=5ihtX86JzmA&t=1m14s">
<img src="assets/ricky.jpg" style="background:none; border:none; box-shadow:none;" class="fragment">
</a>

---

### Your turn!


* &shy;<!-- .element: class="fragment" -->
Write a small script that prints the following to *STDOUT*:
  * &shy;<!-- .element: class="fragment" -->Hello World!
  * &shy;<!-- .element: class="fragment" -->An empty newline
  * &shy;<!-- .element: class="fragment" -->Your **username** 
  * &shy;<!-- .element: class="fragment" -->An empty newline
  * &shy;<!-- .element: class="fragment" -->The path to your current directory
* &shy;<!-- .element: class="fragment" -->How could you save that information in a file?
* &shy;<!-- .element: class="fragment" -->How could you read your script and print only the "Shebang" (#!) lines?

|||

### Your turn

```bash
# SPOILER ALERT!
# Scroll below for the solution














#!/bin/bash
echo "Hello World!"
echo ""
echo $USER
echo ""
pwd

```

|||

### Your turn

```bash
# SPOILER ALERT!
# Scroll below for the solution














chmod +x my_script.sh
./my_script.sh
./my_script.sh > my_text.txt
./my_script |grep "^!#"
```

---

### Privileges


* &shy;<!-- .element: class="fragment" -->Remember that users can only **write** on their *home* dir?
* &shy;<!-- .element: class="fragment" -->What if we need to write files somewhere else?
  * &shy;<!-- .element: class="fragment" -->Only the `root` user can do that
* &shy;<!-- .element: class="fragment" -->You need to either become `root`...
* &shy;<!-- .element: class="fragment" -->...or run a command with `root` privileges
* &shy;<!-- .element: class="fragment" style="color:red"-->For security reasons, you should not use the `root` account for any longer then strictly necessary

&shy;<!-- .element: class="fragment" -->![Groot](assets/groot.jpg)

---

### Escalating privileges

* &shy;<!-- .element: class="fragment" -->In order to become `root` you have to switch to the `root` "user"
  * &shy;<!-- .element: class="fragment" -->Just use `su root` (or the short form `su`)
* &shy;<!-- .element: class="fragment" -->This will not work on Ubuntu by default
* &shy;<!-- .element: class="fragment" -->The `root` account is *disabled* 
* &shy;<!-- .element: class="fragment" -->How do we do this?

&shy;<!-- .element: class="fragment" -->![Spider & Deadpool](assets/how.jpg)

---

### sudo

* &shy;<!-- .element: class="fragment" -->`sudo` is the answer
  * &shy;<!-- .element: class="fragment" -->Stands for "superuser do"
* &shy;<!-- .element: class="fragment" -->`sudo <command>` 
  * &shy;<!-- .element: class="fragment" -->Allows an *unprivileged* user to run a command with elevated privileges
  * &shy;<!-- .element: class="fragment" -->Asks for the user authentication
* &shy;<!-- .element: class="fragment" -->Provided said user is *authorized* to use `sudo`
* &shy;<!-- .element: class="fragment" -->Usually authorized users are part of the `sudo` group
* &shy;<!-- .element: class="fragment" style="color:red" -->`sudo` can be considered a weapon of mass destruction if used carelessly
* &shy;<!-- .element: class="fragment" -->[Mandatory XKCD](https://www.xkcd.com/149/) 

---

### Updating our system

* &shy;<!-- .element: class="fragment" -->An example of a task that requires elevated privileges is performing an update to our system
* &shy;<!-- .element: class="fragment" -->You have to write the updated files to system directories
  * &shy;<!-- .element: class="fragment" -->The commands are:
    * &shy;<!-- .element: class="fragment" -->apt update
    * &shy;<!-- .element: class="fragment" -->apt dist-upgrade
  * &shy;<!-- .element: class="fragment" -->But your "unprivileged" user cannot do this...

|||

### Updating our system

```bash
# SPOILER ALERT!
# Scroll below for the solution














sudo apt update
sudo apt dist-upgrade
```

---

### Package managers!

---

### What does a package manager do?


* &shy;<!-- .element: class="fragment" -->The "correct" way to manage programs on a GNU/Linux system
* &shy;<!-- .element: class="fragment" -->They will handle all of the process
  * &shy;<!-- .element: class="fragment" -->Installing programs
  * &shy;<!-- .element: class="fragment" -->Removing programs
  * &shy;<!-- .element: class="fragment" -->Updating programs
  * &shy;<!-- .element: class="fragment" -->Keep track of all this activity

---

### Package manager concepts


* &shy;<!-- .element: class="fragment" -->Packages
  * &shy;<!-- .element: class="fragment" -->Single file containing the software and metadata
  * &shy;<!-- .element: class="fragment" -->`.deb`, `.rpm`, `.tar.xz`
* &shy;<!-- .element: class="fragment" -->Dependencies
  * &shy;<!-- .element: class="fragment" -->Some packages need others to work
  * &shy;<!-- .element: class="fragment" -->Eg. `numpy` depends on `python` 
* &shy;<!-- .element: class="fragment" -->Package database keeps track of:
  * &shy;<!-- .element: class="fragment" -->All installed packages/files
  * &shy;<!-- .element: class="fragment" -->All available packages
  * &shy;<!-- .element: class="fragment" -->All installed packages' metadata
* &shy;<!-- .element: class="fragment" -->Repositories
  * &shy;<!-- .element: class="fragment" -->Store remote packages
  * &shy;<!-- .element: class="fragment" -->Store remote databases

---

### Using a package manager

* DB-sync <!-- .element: class="fragment" data-fragment-index="1" -->
* Update packages <!-- .element: class="fragment" data-fragment-index="2" -->
* Install packages <!-- .element: class="fragment" data-fragment-index="3" -->
* Remove packages <!-- .element: class="fragment" data-fragment-index="4" -->

|||

### Using a package manager

```bash
# SPOILER ALERT!
# Scroll below for the solution















sudo apt update  # DB-sync

sudo apt dist-upgrade  # Update pacakges

sudo apt install vim  # Installs Vim
sudo apt install ruby nodejs # Installs ruby and node


# Install xeyes
sudo apt install x11-apps

# Test it
xeyes

# Uninstall it
sudo apt remove x11-apps
```

---

### Plot twist #2:

* Now that you have installed `ruby` and `node`, try to run the scripts that previously failed

---

### User space package managers

* pip <!-- .element: class="fragment" data-fragment-index="1" -->
* cran <!-- .element: class="fragment" data-fragment-index="2" -->
* npm <!-- .element: class="fragment" data-fragment-index="3" -->
* conda <!-- .element: class="fragment" data-fragment-index="4" -->

---

### Focus on [conda](https://conda.io)

* &shy;<!-- .element: class="fragment" -->A package, dependency and environment manager for any language
* &shy;<!-- .element: class="fragment" -->Used to install packages, without requiring root access
* &shy;<!-- .element: class="fragment" -->Can be used to manage isolated environments
* &shy;<!-- .element: class="fragment" -->[Simple to install](https://docs.conda.io/projects/conda/en/latest/user-guide/install/linux.html) 
* &shy;<!-- .element: class="fragment" -->[Bioconda](https://bioconda.github.io/) 

---

### Your turn

* &shy;<!-- .element: class="fragment" -->Install `conda` in your VM
* &shy;<!-- .element: class="fragment" -->Add the `Bioconda` channel to the "repositories"
* &shy;<!-- .element: class="fragment" -->Install the program `fastqc` 

---

### Homework

* Create your own VM
  * Install **any** GNU/Linux based distro on it, **except \*buntu**
* Install and run the program [screenfetch](https://github.com/KittyKatt/screenFetch)
* Send me 3 files:
  * A screenshot of your VM with two terminal windows open
    * One with the results of *screenfetch* displayed
    * Another one with the contents of the file `/etc/fstab`
  * A text file containing the output of *screenfetch*
  * A text file with the contents of your VM's `/etc/fstab`
* Both files should be named with your name and student number:
  * `firstname_surname_0000_screenshot.png`
  * `firstname_surname_0000_screenfetch.txt`
  * `firstname_surname_0000_fstab.txt`
* Due date: exactly 2 weeks from now

---

### References

* [Shell scripting intro](https://www.howtogeek.com/67469/the-beginners-guide-to-shell-scripting-the-basics/)
* [An intro to `tar`](https://www.math.utah.edu/docs/info/tar_2.html)
* [Permissions](https://wiki.archlinux.org/index.php/File_permissions_and_attributes)
* [Quick guide on `sudo`](https://www.linux.com/tutorials/linux-101-introduction-sudo/)
* [miniconda](https://docs.conda.io/en/latest/miniconda.html)
* [Bioconda installation](https://bioconda.github.io/#usage)
